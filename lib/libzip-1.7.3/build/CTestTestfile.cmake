# CMake generated Testfile for 
# Source directory: /home/lucas/Desktop/soii-2021-ipc-lmonsierra/lib/libzip-1.7.3
# Build directory: /home/lucas/Desktop/soii-2021-ipc-lmonsierra/lib/libzip-1.7.3/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("lib")
subdirs("man")
subdirs("src")
subdirs("regress")
subdirs("examples")
