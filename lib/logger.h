//
// Created by lucas on 13/4/21.
//

#ifndef SOII_2021_IPC_LMONSIERRA_LOGGER_H
#define SOII_2021_IPC_LMONSIERRA_LOGGER_H


void logger(const int16_t src, const char *ip, const uint16_t port, const char *message);

#endif //SOII_2021_IPC_LMONSIERRA_LOGGER_H
