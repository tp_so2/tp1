CC = gcc
CFLAGS = -Werror -Wall -pedantic -Wextra -Wunused -Wconversion -std=gnu11 -D _GNU_SOURCE

default: all

all: delivery_manager socket productor

delivery_manager:
	$(CC) $(CFLAGS) -lrt -pthread -o bin/delivery_manager src/delivery_manager.c -lcrypto -lssl -lzip -L./lib/libzip-1.7.3

socket:
	$(CC) $(CFLAGS) -o bin/socket src/socket.c -lcrypto -lssl

productor:
	$(CC) $(CFLAGS) -lrt -o bin/productor src/productor.c

clean:
	rm -rf delivery_manager
	rm -rf socket
	rm -rf productor

deleteQueues:
	ls -l /dev/mqueue/
	rm -rf /dev/mqueue/1
	rm -rf /dev/mqueue/2
	rm -rf /dev/mqueue/3
	ls -l /dev/mqueue/