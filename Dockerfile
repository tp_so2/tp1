FROM alpine:3.13

RUN set -ex && \
    apk add --no-cache gcc musl-dev

RUN set -ex && \
    rm -f /usr/libexec/gcc/x86_64-alpine-linux-musl/6.4.0/cc1obj && \
    rm -f /usr/libexec/gcc/x86_64-alpine-linux-musl/6.4.0/lto1 && \
    rm -f /usr/libexec/gcc/x86_64-alpine-linux-musl/6.4.0/lto-wrapper && \
    rm -f /usr/bin/x86_64-alpine-linux-musl-gcj

RUN apk upgrade --update-cache --available && \
    apk add --no-cache --upgrade bash && \
    apk add make && \
    apk add openssl && \
    apk add openssl-dev && \
    rm -rf /var/cache/apk/*
    
WORKDIR /tp1

COPY . .


EXPOSE 4042
