//
// Created by lucas on 5/4/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <time.h>
#include <arpa/inet.h>

#include <sys/socket.h>
#include <errno.h>

#include <zconf.h>
#include <fcntl.h>

#include <mqueue.h>

#include <sys/epoll.h>
#include <pthread.h>

#include <signal.h>
#include <zip.h>
#include <openssl/md5.h>

#include "../src/logger.c"
#include "../src/sllist.c"

#define QUEUE_PERMISSIONS       0660
#define MAX_MESSAGES            10
#define MAX_MSG_SIZE            1024
#define MAX_LOG_SIZE            MAX_MSG_SIZE - MD5_DIGEST_LENGTH
#define MSG_BUFFER_SIZE         MAX_MSG_SIZE + 10
#define MAX_CONNECT             5000
#define MAX_IDLE_TIME           5
#define PORT_NUMBER             5555
#define PARAMS                  3
#define CMD_LIMIT               30
#define PORT_MIN                1023
#define PORT_MAX                65536
#define ADD_CMD                 "add"
#define DEL_CMD                 "delete"
#define LOG_CMD                 "log"
#define LOG_KEY                 "log$"
#define MD5_KEY                 "<md5>"

/* Variables globales */
FILE *fp;
int32_t socketFd, clientFd, threadFd, epFd, nFds;
mqd_t qdProd1, qdProd2, qdProd3;
struct sockaddr_in clientAddress;
struct epoll_event ev, events[MAX_CONNECT];
struct sllist *prod1, *prod2, *prod3, *susc;
pthread_t threadId;

/* Estructura de suscriptor */
/**
 * Estructura de un suscriptor
 * @attr fd file descriptor del socket del suscriptor
 * @attr timestamp marca de tiempo en que se desconectó el suscriptor
 * @attr ip ip del suscriptor
 * @attr port puerto del suscriptor
 */
struct suscriptor {
    int32_t fd;
    time_t timestamp;
    char *ip;
    uint16_t port;
    struct sllist *queuedMsg;
    int16_t status;
};

struct msg {
    int16_t prod;
    char *buffer;
};

/* Declaración de funciones */
void setupQueues();

int32_t queueCreation();

int32_t registerProd1();

int32_t registerProd2();

int32_t registerProd3();

static void queueNotify(union sigval sv);

mqd_t getMqd(int16_t prodNum);

void sendMsg(char *buffer, int16_t prod);

int32_t sendAndLog(char *buffer, struct suscriptor *s, int16_t prod);

int32_t reRegisterProd(int16_t prodNum);

void *deliveryManager();

void serverUp();

int32_t socketBind();

int32_t acceptConnection();

int32_t checkConnection();

int32_t tryConnection();

void setupServerEpoll();

void setupClientEpoll(int32_t fd);

void verifyEventList();

void printWelcomeMsg();

void printAvailableCmd();

void readCmd(char *cmd, int32_t *argc, char *argv[]);

int32_t checkArgs(const int32_t *argc, char *cmd, char *ip, char *port, char *prod);

int32_t checkAddDelCmd(const int32_t *argc, char *ip, char *port, char *prod);

int32_t checkLogCmd(const int32_t *argc, char *ip, char *port);

int32_t checkIpAddress(char *ipAddress);

int32_t checkPortNumber(char *port);

int32_t checkProd(char *prod);

void addSusc(char *ip, uint16_t port, uint16_t prod);

void delSusc(char *ip, uint16_t port, uint16_t prod);

void sendLog(char *ip, uint16_t port);

int32_t compressLog();

struct suscriptor *findSuscByFd(int32_t fd, struct sllist *list);

struct suscriptor *findSuscByIpPort(char *ip, uint16_t port, struct sllist *list);

int32_t delSuscByIpPort(char *ip, uint16_t port, struct sllist *list);

char *generateMD5(char *buffer);

int32_t registerExitHandlers();

static void exitHandlerQueues(void);

static void exitHandlerLists(void);

static void exitHandlerSocket(void);

void intHandler(int sig);

/* Función principal */
int32_t main() {
    errno = EXIT_SUCCESS;
    /* Registro los exit handlers */
    if (registerExitHandlers()) {
        printf("Ocurrió un error al hacer el registro de los exit handlers: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Inicializo las listas de productores y suscriptores */
    prod1 = sllist_create();
    prod2 = sllist_create();
    prod3 = sllist_create();
    susc = sllist_create();


    /* Crea y registra las notificaciones de las colas de mensajes */
    setupQueues();

    /* Inicio Delivery manager */
    if ((threadFd = pthread_create(&threadId, NULL, deliveryManager, NULL)) != 0) {
        printf("Ocurrió un error al iniciar el delivery manager en un nuevo hilo: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    printWelcomeMsg();

    while (1) {
        int32_t *argc = (int32_t *) calloc(1, sizeof(int32_t));
        char *cmd = calloc(CMD_LIMIT, sizeof(char));
        char **argv = calloc(PARAMS, PARAMS * sizeof(char *));

        /* Leo los comandos ingresados por consola */
        readCmd(cmd, argc, argv);

        /* Decodifico los argumentos */
        char *ip = strtok(argv[1], ":");
        char *port = strtok(NULL, " ");
        char *prod = argv[*argc - 1];

        /* Verifico los argumentos pasados en el comando ingresado */
        if (checkArgs(argc, argv[0], ip, port, prod) == 0) {
            if (strcmp(argv[0], ADD_CMD) == 0) {
                /* Agrego el suscriptor a la lista del productor */
                addSusc(ip, (uint16_t) strtol(port, NULL, 10), (uint16_t) strtol(prod, NULL, 10));
            } else if (strcmp(argv[0], DEL_CMD) == 0) {
                delSusc(ip, (uint16_t) strtol(port, NULL, 10), (uint16_t) strtol(prod, NULL, 10));
            } else {
                sendLog(ip, (uint16_t) strtol(port, NULL, 10));
            }
        }

        free(cmd);
        free(argc);
        free(argv);
    }
}

/**
 * Realiza todos los pasos para crear y registrar las notificaciones de las colas de mensajes
 */
void setupQueues() {
    /* Creo las colas de mensajes */
    if (queueCreation()) {
        printf("Ocurrió un error durante la creación de las colas de mensajes: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Registro las notificaciones a las colas */
    if (registerProd1() || registerProd2() || registerProd3()) {
        printf("Ocurrió un error durante el registro de notificaciones: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
 * Creo las colas de mensajes para los distintos productores
 * Se crean como sólo lectura y se activan las notificaciones para ser manejadas
 * por la función notifyHandler()
 * @return 0 en caso de éxito, el valor del errno en su defecto
 */
int32_t queueCreation() {
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MESSAGES;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;

    if ((qdProd1 = mq_open("/1", O_RDONLY | O_CREAT | O_NONBLOCK, QUEUE_PERMISSIONS, &attr)) == -1) {
        printf("Ocurrió un error al crear la cola de mensajes [1]");
    }
    if ((qdProd2 = mq_open("/2", O_RDONLY | O_CREAT | O_NONBLOCK, QUEUE_PERMISSIONS, &attr)) == -1) {
        printf("Ocurrió un error al crear la cola de mensajes [2]");
    }
    if ((qdProd3 = mq_open("/3", O_RDONLY | O_CREAT | O_NONBLOCK, QUEUE_PERMISSIONS, &attr)) == -1) {
        printf("Ocurrió un error al crear la cola de mensajes [3]");
    }

    return errno;
}

/**
 * Registra las notificaciones de la cola de mensajes del productor 1
 * @return 0 en caso de éxito, el valor del errno en su defecto
 */
int32_t registerProd1() {
    struct sigevent sev;
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_attributes = NULL;
    sev.sigev_notify_function = queueNotify;
    sev.sigev_value.sival_int = 1;

    if (mq_notify(qdProd1, &sev) == -1) {
        printf("Ocurrió un error al registrar las notificaciones para el productor 1\n");
    }

    return errno;
}

/**
 * Registra las notificaciones de la cola de mensajes del productor 2
 * @return 0 en caso de éxito, el valor del errno en su defecto
 */
int32_t registerProd2() {
    struct sigevent sev;
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_attributes = NULL;
    sev.sigev_notify_function = queueNotify;
    sev.sigev_value.sival_int = 2;

    if (mq_notify(qdProd2, &sev) == -1) {
        printf("Ocurrió un error al registrar las notificaciones para el productor 2\n");
    }

    return errno;
}

/**
 * Registra las notificaciones de la cola de mensajes del productor 3
 * @return 0 en caso de éxito, el valor del errno en su defecto
 */
int32_t registerProd3() {
    struct sigevent sev;
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_attributes = NULL;
    sev.sigev_notify_function = queueNotify;
    sev.sigev_value.sival_int = 3;

    if (mq_notify(qdProd3, &sev) == -1) {
        printf("Ocurrió un error al registrar las notificaciones para el productor 3\n");
    }

    return errno;
}

/**
 * Handler de notificaciones de la cola de mensajes
 * @param signal value datos pasados con la notificación
 */
static void queueNotify(union sigval sv) {
    char *buffer;
    struct mq_attr attr;
    int16_t prodNum = (int16_t) sv.sival_int;
    mqd_t mqd = getMqd(prodNum);

    if (mq_getattr(mqd, &attr) == -1) {
        fprintf(stderr, "Ocurrió un error al obtener los atributos de la cola de mensajes [%i]\n", prodNum);
        exit(EXIT_FAILURE);
    }

    if ((buffer = calloc((long unsigned int) attr.mq_msgsize, sizeof(char))) == NULL) {
        fprintf(stderr, "Ocurrió un error al alocar memoria para el buffer de recepción [%i]\n", prodNum);
        exit(EXIT_FAILURE);
    }

    if (mq_receive(mqd, buffer, MSG_BUFFER_SIZE, 0) == -1) {
        fprintf(stderr, "Ocurrió un error al recibir el mensaje del productor [%i]\n", prodNum);
        exit(EXIT_FAILURE);
    }

    /* Envío el mensaje a los suscriptores */
    sendMsg(buffer, prodNum);

    free(buffer);

    /* Registro las notificaciones a la cola que generó la notificación */
    if (reRegisterProd(prodNum)) {
        printf("Ocurrió un error durante el registro de notificaciones: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
 * Obtiene el descriptor de la cola de mensajes a partir del id del productor
 * @param prodNum número de identificador del productor
 * @return mdq descriptor de la cola de mensajes del productor solicitado
 */
mqd_t getMqd(int16_t prodNum) {
    switch (prodNum) {
        case 1:
            return qdProd1;
        case 2:
            return qdProd2;
        case 3:
            return qdProd3;
        default:
            fprintf(stderr, "Error al obtener el descriptor de la cola [%i]\n", prodNum);
            return -1;
    }
}

/**
 * Realiza el envío de mensajes a la lista de suscriptores correspondientes
 * Además verifica la validez del suscriptor con su timestamp
 * @param buffer mensaje a enviar
 * @param prod número del productor del cuál proviene el mensaje
 * @return 0 en caso de exito, mayor a 0 en su defecto
 */
void sendMsg(char *buffer, int16_t prod) {
    /* Selecciono la lista del productor correspondiente */
    struct sllist *prodList;

    switch (prod) {
        case 1:
            prodList = prod1;
            break;
        case 2:
            prodList = prod2;
            break;
        case 3:
            prodList = prod3;
            break;
        default:
            printf("El productor %i no está disponible para enviar mensajes\n", prod);
            return;
    }

    /* Recorro la lista del productor correspondiente */
    struct suscriptor *s;
    for (int i = 0; i < prodList->size; i++) {
        if ((s = sllist_read_index(prodList, i)) == NULL) {
            continue;
        }

        /* Verifico si el suscriptor es válido */
        if ((s->timestamp != 0) && ((time(NULL) - s->timestamp) >= MAX_IDLE_TIME)) {
            /* Se venció el tiempo de espera. Elimino el suscriptor */
            sllist_extract(prodList, i);
            delSuscByIpPort(s->ip, s->port, susc);
            i--;
        } else {
            /* Suscriptor válido, genero el MD5 y envío el mensaje */
            if (sendAndLog(buffer, s, prod) == 0) {
                /* Verifico si hay mensajes encolados */
                while (s->queuedMsg->size > 0) {
                    struct msg *m = sllist_pop_front(s->queuedMsg);
                    sendAndLog(m->buffer, s, m->prod);
                }
            }
        }
    }
}

/**
 * Realiza el calculo de MD5, envía el mensaje si el socket está disponible, sino lo encola
 * Encaso de estar disponible el socket, verifica si no tiene mensajes encolados que deban ser enviados
 * @param buffer mensaje a enviar
 * @param s suscriptor al que se debe enviar el mensaje
 * @param prod número del productor del cuál proviene el mensaje
 * @return 0 en caso de éxito, 1 en caso no haber podido enviar el mensaje (se encola el mensaje)
 */
int32_t sendAndLog(char *buffer, struct suscriptor *s, int16_t prod) {
    char *msg = calloc(MAX_MSG_SIZE, sizeof(char));
    strcpy(msg, buffer);
    generateMD5(msg);
    if (s->status != 1) {
        /* Suscriptor desconectado. Encolo el mensaje */
        printf("No se pudo enviar el mensaje al suscriptor <%s:%i>, está desconectado\n", s->ip, s->port);
        /* Encolo el mensaje */
        struct msg *m = calloc(1, sizeof(struct msg));
        m->prod = prod;
        m->buffer = buffer;
        sllist_push_front(s->queuedMsg, m);
        free(msg);
        return 1;
    } else {
        if (send(s->fd, msg, strlen(msg), 0) == -1) {
            /* Error al enviar el mensaje. Encolo el mensaje */
            printf("No se pudo enviar el mensaje del productor %i al suscriptor <%s:%i>\n", prod, s->ip, s->port);
            struct msg *m = calloc(1, sizeof(struct msg));
            m->prod = prod;
            m->buffer = buffer;
            sllist_push_front(s->queuedMsg, m);
            errno = EXIT_SUCCESS;
            free(msg);
            return 1;
        } else {
            logger(prod, s->ip, s->port, buffer);
            free(msg);
            return 0;
        }
    }
}

/**
 * Registra nuevamente las notificaciones para la cola de mensajes del productor especificado
 * @param prodNum número de identificador del productor
 * @return 0 en caso de éxito, el valor del errno en su defecto
 */
int32_t reRegisterProd(int16_t prodNum) {
    switch (prodNum) {
        case 1:
            return registerProd1();
        case 2:
            return registerProd2();
        case 3:
            return registerProd3();
        default:
            fprintf(stderr, "Error al obtener el descriptor de la cola [%i]\n", prodNum);
            return errno;
    }
}

/**
 * Realiza el manejo completo de la comunicación entre los productores y los suscriptores
 */
void *deliveryManager() {
    /* Levanto el socket del servidor */
    serverUp();

    /* Creo una nueva instancia de epoll */
    if ((epFd = epoll_create1(0)) == -1) {
        printf("Ocurrió un error al crear la nueva instancia de epoll: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Configuro la función epoll para el servidor */
    setupServerEpoll();

    /* Bucle para el manejo de nuevas conexiones con epoll */
    while (1) {
        /* Espero a que suceda un evento registrado en epoll */
        // timeout = -1, epoll se bloquea indefinidamente hasta que suceda un evento
        if ((nFds = epoll_wait(epFd, events, MAX_CONNECT, -1)) == -1) {
            printf("Ocurrió un error al capturar el nuevo evento epoll_wait: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        /* Verifico la lista de eventos */
        verifyEventList();
    }
}

/**
 * Realiza todos los pasos para levantar el socket del servidor
 */
void serverUp() {
    /* Creo FD para Socket TCP en IPv4 */
    if ((socketFd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Ocurrió un error al crear el socket: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Bindeo el Socket del servidor */
    if (socketBind()) {
        printf("Ocurrió un error al bindear el socket: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Activo escucha pasiva del Socket */
    if (listen(socketFd, MAX_CONNECT)) {
        printf("Ocurrió un error al iniciar la espera pasiva del socket: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    printf("Proceso [%d] escuchando en el puerto: [%i]\n", getpid(), PORT_NUMBER);
}

/**
 * Realiza el bind del socket
 * @return 0 en caso de éxito, -1 en caso contrario
 */
int32_t socketBind() {
    struct sockaddr_in *serverAddress = calloc(1, sizeof(struct sockaddr_in));
    serverAddress->sin_family = AF_INET;
    serverAddress->sin_port = htons(PORT_NUMBER);
    serverAddress->sin_addr.s_addr = INADDR_ANY;
    return bind(socketFd, (struct sockaddr *) serverAddress, sizeof(struct sockaddr));
}

/**
 * Acepta nuevas conexiones TCP y guarda los datos del cliente en la estructura clientAddress
 * @return 0 si se aceptó correctamente, 1 en caso contrario
 */
int32_t acceptConnection() {
    int32_t lenghtClient = (int32_t) sizeof(struct sockaddr_in);
    clientFd = accept(socketFd, (struct sockaddr *) &clientAddress, (socklen_t *) &lenghtClient);
    return clientFd < 0;
}

/**
 * Envía un mensaje de prueba para testear la conexión con el servidor
 * @return 0 en caso de éxtio, o 1 en caso contrario
 */
int32_t checkConnection() {
    char *buffer = calloc(MAX_MSG_SIZE, sizeof(char));
    int32_t valRead = (int32_t) recv(clientFd, buffer, MAX_MSG_SIZE, 0);
    if (valRead < 1) {
        fprintf(stderr, "Error al verificar la conexión\n");
    } else {
        printf("TEST CONNECTION [%i]: %s\n", valRead, buffer);
        send(clientFd, buffer, strlen(buffer), 0);
    }
    return errno || valRead < 1;
}

/**
 * Realiza todos los pasos para configurar el polling del servidor para determinar cuándo un suscriptor se conecta
 * https://man7.org/linux/man-pages/man7/epoll.7.html
 */
void setupServerEpoll() {
    // EPOLLIN el archivo asociado está listo para ser leído
    ev.data.fd = socketFd;
    ev.events = EPOLLIN;

    /* Configuro epoll para que escuche el socket del servidor */
    if (epoll_ctl(epFd, EPOLL_CTL_ADD, socketFd, &ev) == -1) {
        printf("Ocurrió un error al asociar el servidor a la lista de epoll: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
 * Realiza todos los pasos para configurar el polling de un cliente para determinar cuándo un suscriptor se desconecta
 * https://man7.org/linux/man-pages/man7/epoll.7.html
 */
void setupClientEpoll(int32_t fd) {
    // EPOLLOUT el archivo asociado está listo para ser escrito
    // EPOLLRDHUP el cliente perdió conexión
    ev.data.fd = fd;
    ev.events = EPOLLRDHUP;

    /* Configuro epoll para que escuche el socket del cliente */
    if (epoll_ctl(epFd, EPOLL_CTL_ADD, fd, &ev) == -1) {
        printf("Ocurrió un error al asociar el cliente [%i] a la lista de epoll: %s\n", fd, strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
* Realiza toda la verificación de los eventos involucrados con la función epoll, tanto del servidor como los cientes
*/
void verifyEventList() {
    for (int i = 0; i < nFds; ++i) {
        /* Evento en el socket del servidor -> Nueva conexión */
        if (events[i].data.fd == socketFd) {
            /* Acepto nuevas conexiones TCP */
            if (acceptConnection()) {
                printf("Ocurrió un error al aceptar una nueva conexión: %s\n", strerror(errno));
            }
            /* Verifico conexión con el cliente */
            if (checkConnection()) {
                printf("Ocurrió un error al testear la conexión: %s\n", strerror(errno));
                close(clientFd);
            } else {
                /* Incluyo el nuevo suscriptor */
                char *ip = inet_ntoa(clientAddress.sin_addr);
                uint16_t port = ntohs(clientAddress.sin_port);
                printf("Nuevo suscriptor FD[%i] - [%s]:[%i]\n", clientFd, ip, port);

                /* Configuro la función epoll para el servidor */
                setupClientEpoll(clientFd);

                struct suscriptor *ns = findSuscByIpPort(ip, port, susc);
                if (ns == NULL) {
                    /* Nuevo suscriptor. Lo agrego a la lista */
                    ns = calloc(1, sizeof(struct suscriptor));
                    sllist_push_front(susc, ns);
                    ns->queuedMsg = sllist_create();
                }   /* Actualizo los datos. Suscriptor reconectado */
                ns->fd = clientFd;
                ns->ip = ip;
                ns->port = port;
                ns->timestamp = 0;
                ns->status = 1;
            }
        } else {
            /* Evento en el socket de un suscriptor */
            if (events[i].events == EPOLLRDHUP) { /* Suscriptor desconectado */
                struct suscriptor *s;
                if ((s = findSuscByFd(events[i].data.fd, prod1)) != NULL) {
                    s->timestamp = time(NULL);
                    s->status = 0;
                } else if ((s = findSuscByFd(events[i].data.fd, prod2)) != NULL) {
                    s->timestamp = time(NULL);
                    s->status = 0;
                } else if ((s = findSuscByFd(events[i].data.fd, prod3)) != NULL) {
                    s->timestamp = time(NULL);
                    s->status = 0;
                } else {
                    printf("Suscriptor desconetado no encontrado\n");
                }
                close(events[i].data.fd);
            } else {
                printf("Ocurrió un evento no manejado en el suscriptor\n");
                close(events[i].data.fd);
            }
        }
    }
}

/**
 * Imprime en pantalla un mensaje de bienvenida
 */
void printWelcomeMsg() {
    printf("\n");
    printf("--------------------------------------------------------------------------\n");
    printf("|                               Bienvenido                               |\n");
    printf("--------------------------------------------------------------------------\n");
    printAvailableCmd();
}

/**
 * Imprime en pantalla u los comandos disponibles
 */
void printAvailableCmd() {
    printf("\n");
    printf("--------------------------------------------------------------------------\n");
    printf("|                          Comandos disponibles                          |\n");
    printf("--------------------------------------------------------------------------\n");
    printf("| add <ip:puerto> <prod>    -> Suscribe socket al porductor indicado.    |\n");
    printf("| delete <ip:puerto> <prod> -> Desuscribe socket del porductor indicado. |\n");
    printf("| log <ip:puerto>           -> Envía el log local al socket indicado.    |\n");
    printf("--------------------------------------------------------------------------\n");
    printf("\n");
}

/**
 * Realiza la lectura de los comandos ingresados por consola
 */
void readCmd(char *cmd, int32_t *argc, char *argv[]) {
    /* Espero que el usuario ingrese un comando */
    printf("\nIngrese un comando: ");
    fgets(cmd, CMD_LIMIT, stdin);

    /* Hago split del comando */
    argv[*argc] = strtok(cmd, " ");
    while (argv[*argc] != NULL) {
        *argc += 1;
        argv[*argc] = strtok(NULL, " ");
    }
}

/**
 * Verifica validez de los argumentos enviados
 * @param argc cantidad de argumentos
 * @param cmd comando a ejecutar. (add|delete|log)
 * @param ip dirección ip a validar
 * @param port número de puerto a validar
 * @param prod número de productor a validar
 * @return 0 si los argumentos son correctos, distinto de 0 en caso contrario
 */
int32_t checkArgs(const int32_t *argc, char *cmd, char *ip, char *port, char *prod) {
    if (*argc > PARAMS) {
        printAvailableCmd();
        return 1;
    } else {
        if ((strcmp(cmd, ADD_CMD) == 0) || (strcmp(cmd, DEL_CMD) == 0)) {
            return checkAddDelCmd(argc, ip, port, prod);
        } else if (strcmp(cmd, LOG_CMD) == 0) {
            return checkLogCmd(argc, ip, port);
        } else {
            printAvailableCmd();
            return 1;
        }
    }
}

/**
 * Verifica validez de los argumentos para los comandos ADD o DELETE
 * @param argc cantidad de argumentos
 * @param ip dirección ip a valdar
 * @param port número de puerto a validar
 * @param número de productor a validar
 * @return 0 si es válido, distinto a 0 en caso contrario
 */
int32_t checkAddDelCmd(const int32_t *argc, char *ip, char *port, char *prod) {
    int32_t flag = 0;
    if (*argc != PARAMS) {
        printAvailableCmd();
        flag += 1;
    } else {
        flag += checkIpAddress(ip);
        flag += checkPortNumber(port);
        flag += checkProd(prod);
    }
    return flag;
}

/**
 * Verifica validez de los argumentos para el comando LOG
 * @param argc cantidad de argumentos
 * @param ip dirección ip a valdar
 * @param port número de puerto a validar
 * @return 0 si es válido, mayor a 0 en caso contrario
 */
int32_t checkLogCmd(const int32_t *argc, char *ip, char *port) {
    int32_t flag = 0;
    if (*argc != (PARAMS - 1)) {
        printAvailableCmd();
        flag += 1;
    } else {
        flag += checkIpAddress(ip);
        flag += checkPortNumber(port);
    }
    return flag;
}

/**
 * Verifica validez de la dirección IP
 * @param ipAddress dirección IPv4
 * @return 0 si es válida, 1 en caso contrario
 */
int32_t checkIpAddress(char *ipAddress) {
    struct in_addr buf[sizeof(struct in_addr)];
    int32_t flag = inet_pton(AF_INET, ipAddress, &buf);;
    if (flag != 1) {
        fprintf(stderr, "Diección IP inválida. Sólo se aceptan direcciones con formato IPv4\n");
    }
    return !flag;
}

/**
 * Verifica validez del número de puerto
 * @param port número de puerto
 * @return 0 si es válido, 1 en caso contrario
 */
int32_t checkPortNumber(char *port) {
    if (port != NULL) {
        u_int16_t portNumber = (u_int16_t) strtol(port, NULL, 10);
        if (portNumber <= PORT_MIN) {
            fprintf(stderr, "Número de puerto inválido. %i < <port_num> < %i\n", PORT_MIN, PORT_MAX);
        }
        return portNumber <= PORT_MIN;
    } else {
        fprintf(stderr, "Formato de socket inválido. El formato es <ip:puerto>\n");
        return 1;
    }
}

/**
 * Verifica validez del argumento productor
 * @param prod productor
 * @return 0 si es válida, 1 en caso contrario
 */
int32_t checkProd(char *prod) {
    u_int16_t numpProd = (u_int16_t) strtol(prod, NULL, 10);
    if (errno != 0) {
        printf("Errno: %s\n", strerror(errno));
    }
    if (numpProd < 1 || numpProd > 3) {
        fprintf(stderr, "Productor inválido. Sólo están implementados los productores 1, 2 y 3\n");
    }
    return errno != 0 || numpProd < 1 || numpProd > 3;
}

/**
 * Agrega el suscriptor a la lista del productor especificado
 * @param ip dirección ip del suscriptor a agregar
 * @param port número de puerto del suscriptor a agregar
 * @param número de productor a suscribir
 */
void addSusc(char *ip, uint16_t port, uint16_t prod) {
    int32_t flag = 0;
    struct suscriptor *s = findSuscByIpPort(ip, port, susc);

    printf("Agregando el suscriptor <%s:%i> a la lista del productor %i\n", ip, port, prod);

    if (s != NULL) {
        switch (prod) {
            case 1:
                flag = sllist_push_front(prod1, s) < 0;
                break;
            case 2:
                flag = sllist_push_front(prod2, s) < 0;
                break;
            case 3:
                flag = sllist_push_front(prod3, s) < 0;
                break;
            default:
                flag = 1;
                break;
        }
        if (flag) {
            printf("No se pudo agregar correctamente el suscriptor <%s:%i> a la lista del productor %i\n",
                   ip, port, prod);
        }
    } else {
        printf("El suscriptor <%s:%i> no está disponible para ser agregado\n", ip, port);
    }
}

/**
 * Elimina el suscriptor de la lista del productor especificado
 * @param ip dirección ip del suscriptor a eliminar
 * @param port número de puerto del suscriptor a eliminar
 * @param número de productor a desuscribir
 */
void delSusc(char *ip, uint16_t port, uint16_t prod) {
    int32_t flag = 0;
    struct suscriptor *s = findSuscByIpPort(ip, port, susc);

    printf("Eliminando el suscriptor <%s:%i> a la lista del productor %i\n", ip, port, prod);

    if (s != NULL) {
        switch (prod) {
            case 1:
                flag += delSuscByIpPort(ip, port, prod1);
                break;
            case 2:
                flag += delSuscByIpPort(ip, port, prod2);
                break;
            case 3:
                flag += delSuscByIpPort(ip, port, prod3);
                break;
            default:
                flag = 1;
                break;
        }
        if (flag) {
            printf("No se pudo eliminar correctamente el suscriptor <%s:%i> a la lista del productor %i\n",
                   ip, port, prod);
        }
    } else {
        printf("El suscriptor <%s:%i> no está disponible para ser eliminado\n", ip, port);
    }
}

/**
 * Comprime y envía el log local al suscriptor especificado por parámetro
 * @param ip dirección ip del suscriptor a eliminar
 * @param port número de puerto del suscriptor a eliminar
 */
void sendLog(char *ip, uint16_t port) {
    struct suscriptor *s;
    if ((s = findSuscByIpPort(ip, port, susc)) == NULL) {
        printf("El suscriptor <%s:%i> no está disponible para enviar el log\n", ip, port);
    } else {
        /* Comprimo el log y verifico que no se produzcan errores */
        if (compressLog() == 0) {
            /* Abro el log comprimido en modo lectura */
            if ((fp = fopen("../log/dm.log", "r")) == NULL) {
                fprintf(stderr, "Error al abrir el archivo comprimido de log\n");
            } else {
                /* Realizo el envío del archivo en partes */
                char *data = calloc(MAX_LOG_SIZE - strlen(LOG_KEY) - strlen(MD5_KEY), sizeof(char));
                char *buff = calloc(MAX_MSG_SIZE, sizeof(char));
                while (fgets(data, MAX_LOG_SIZE - strlen(LOG_KEY) - strlen(MD5_KEY), fp) != NULL) {
                    sprintf(buff, "%s%s", LOG_KEY, data);
                    generateMD5(buff);
                    if (send(s->fd, buff, MAX_MSG_SIZE, 0) == -1) {
                        printf("No se pudo enviar correctamente el log local suscriptor <%s:%i>\n", ip, port);
                        errno = EXIT_SUCCESS;
                        break;
                    }
                }
                free(buff);
                free(data);
                fclose(fp);
            }
        }


    }
}


/**
 * Realiza toda la lógica para comprimir el archivo de log local
 * @return 0 en caso de éxito, 1 en caso contrario
 */
int32_t compressLog() {
    printf("Comprimiendo el log local\n");
    zip_t *zip;
    zip_source_t *szip;
    struct stat fstat;

    /* Comprimo el archivo de log local */
    if ((zip = zip_open("../log/log.zip", ZIP_CREATE | ZIP_TRUNCATE, NULL)) == NULL) {
        fprintf(stderr, "Ocurrió un error al crear el .zip\n");
        return 1;
    }

    /* Obtengo informacíon del archivo log */
    if (stat("../log/dm.log", &fstat) != 0) {
        fprintf(stderr, "Ocurrió un error al obtener la información sobre el archivo a comprimir\n");
        zip_close(zip);
        return 1;
    }

    /* Comprimo el log */
    if ((szip = zip_source_file(zip, "../log/dm.log", 0, (zip_int64_t) fstat.st_size)) == NULL ||
        (zip_file_add(zip, "../log/dm.log", szip, ZIP_FL_OVERWRITE)) < 0) {
        printf("Ocurrió un error al comprimir el log local\n");
        zip_source_free(szip);
        zip_close(zip);
        return 1;
    }

    zip_close(zip);
    return 0;
}

/**
 * Busca un suscriptor en la lista de suscriptores por su file descriptor
 * @param fd file descriptor del suscriptor a buscar
 * @return suscriptor especificado, NULL en su defecto
 */
struct suscriptor *findSuscByFd(int32_t fd, struct sllist *list) {
    for (int i = 0; i < list->size; i++) {
        if (fd == ((struct suscriptor *) sllist_read_index(list, i))->fd) {
            return sllist_read_index(list, i);
        }
    }
    return NULL;
}

/**
 * Busca un suscriptor en la lista de suscriptores por <ip:puerto>
 * @param ip dirección ip
 * @param port puerto
 * @return suscriptor especificado, NULL en su defecto
 */
struct suscriptor *findSuscByIpPort(char *ip, uint16_t port, struct sllist *list) {
    for (int i = 0; i < list->size; i++) {
        struct suscriptor *s = (struct suscriptor *) sllist_read_index(list, i);
        if ((strcmp(ip, s->ip) == 0) && (port == s->port)) {
            return s;
        }
    }
    return NULL;
}

/**
 * Busca un suscriptor en la lista indicada por <ip:puerto> y lo elimina
 * @param ip dirección ip
 * @param port puerto
 * @param list lista de donde eliminar el suscriptor
 * @return 0 en caso de éxito, distinto de 0 en caso de error
 */
int32_t delSuscByIpPort(char *ip, uint16_t port, struct sllist *list) {
    for (int i = 0; i < list->size; i++) {
        struct suscriptor *s = (struct suscriptor *) sllist_read_index(list, i);
        if ((strcmp(ip, s->ip) == 0) && (port == s->port)) {
            return (sllist_extract(list, i) == NULL);
        }
    }
    return 1;
}

/**
 * Genera el checksum del mensaje a ser enviado con formato <msg|checksum>
 * @param mensaje al cual se le va a calcular el checksum
 */
char *generateMD5(char *buffer) {
    MD5_CTX c;
    unsigned char out[MD5_DIGEST_LENGTH];

    MD5_Init(&c);
    MD5_Update(&c, buffer, strlen(buffer));

    MD5_Final(out, &c);

    strcat(buffer, MD5_KEY);
    strcat(buffer, (char *) out);
    return buffer;
}

/**
 * Realiza toda la lógica para registrar los handlers de exit()
 */
int32_t registerExitHandlers() {
    signal(SIGINT, intHandler);
    return atexit(exitHandlerQueues) || atexit(exitHandlerLists) || atexit(exitHandlerSocket);
}

/**
 * Exit handler para eliminar las colas de mensajes
 */
static void exitHandlerQueues(void) {
    printf("Elimino las colas\n");
    mq_unlink("/1");
    mq_unlink("/2");
    mq_unlink("/3");
}

/**
 * Exit handler para liberar la memoria de las listas y su contenido
 */
static void exitHandlerLists(void) {
    printf("Elimino las listas\n");
    sllist_destroy(susc);
    sllist_destroy(prod1);
    sllist_destroy(prod2);
    sllist_destroy(prod3);
}

/**
 * Exit handler para cerrar el socket del delivery manager
 */
static void exitHandlerSocket(void) {
    printf("Elimino el socket del servidor\n");
    close(socketFd);
}

/**
 * Handler para manejar combinación Ctrl+C
 * https://stackoverflow.com/a/4217070/14311150
 * @param sig señal a ser manejada
 */
void intHandler(int sig) {
    signal(sig, SIG_IGN);
    printf("\nYou hit Ctrl-C. Bye, bye...\n");
    exit(EXIT_SUCCESS);
}