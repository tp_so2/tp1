//
// Created by lucas on 5/4/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <openssl/md5.h>
#include <unistd.h>

#define PARAMS                  3
#define PORT_MIN                1023
#define PORT_MAX                65536
#define MAX_MSG_SIZE            1024
#define LOG_KEY                 "log$"
#define MD5_KEY                 "<md5>"

/* Variables globales */
FILE *fp;
int32_t socketFd;
uint16_t portNumber;
struct in_addr buf[sizeof(struct in_addr)];

/* Declaración de funciones */
void socketSetup(int argc, char *argv[]);

int32_t checkArgs(int32_t argc, char *argv[]);

int32_t checkIpAddress(char *ipAddress);

int32_t checkPortNumber(char *port);

int32_t tryConnection();

int32_t checkConnection();

void printWelcomeMsg();

int32_t receiveMsg();

int32_t checkMD5(char *buffer);

void getLog(char *buffer);

int32_t registerExitHandlers();

static void exitHandlerSocket(void);

void intHandler(int sig);

/* Función principal */
int32_t main(int argc, char *argv[]) {
    /* Registro los exit handlers */
    if (registerExitHandlers()) {
        printf("Ocurrió un error al hacer el registro de los exit handlers: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Conecto el socket del suscriptor */
    socketSetup(argc, argv);

    printWelcomeMsg();

    while (1) {
        /* Recepción de mensajes */
        if (receiveMsg()) {
            printf("Ocurrió un al recibir un mensaje del servidor: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }
}

/**
 * Realiza todos los pasos para conectar el socket del suscriptor
 * @param argc cantidad de argumentos
 * @param argv listado de argumentos
 */
void socketSetup(int argc, char *argv[]) {
    //* Verifico la validez de los argumentos */
    if (checkArgs(argc, argv)) {
        exit(EXIT_FAILURE);
    }

    /* Creo FD para Socket TCP en IPv4 */
    if ((socketFd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Ocurrió un error al crear el socket: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Intento de conexión con el servidor */
    if (tryConnection()) {
        printf("Ocurrió un error al establecer la conexión con el server: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Envío mensaje de prueba */
    if (checkConnection()) {
        printf("Ocurrió un error al establecer la conexión con el server: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}

/**
 * Verifica validez de los argumentos con los que se ejecutó el programa
 * @param argc cantidad de argumentos
 * @param argv listado de argumentos
 * @return 0 si los argumentos son correctos
 */
int32_t checkArgs(int argc, char *argv[]) {
    int32_t flag = 0;
    if (argc != PARAMS) {
        fprintf(stderr, "Formato inválido. ./client <ip_address> <port_num>\n");
        flag++;
    } else {
        flag += checkIpAddress(argv[1]);
        flag += checkPortNumber(argv[2]);
    }
    return flag;
}

/**
 * Verifica validez de la dirección IP
 * @param ipAddress dirección IPv4
 * @return 0 si es válida, 1 en caso contrario
 */
int32_t checkIpAddress(char *ipAddress) {
    int32_t flag = inet_pton(AF_INET, ipAddress, &buf);
    if (flag != 1) {
        fprintf(stderr, "Diección IP inválida. Sólo se aceptan direcciones con formato IPv4\n");
    }
    return !flag;
}

/**
 * Verifica validez del número de puerto
 * @param portNumber número de puerto
 * @return 0 si es válido, 1 en caso contrario
 */
int32_t checkPortNumber(char *port) {
    portNumber = (uint16_t) strtol(port, NULL, 10);
    if (portNumber <= PORT_MIN) {
        fprintf(stderr, "Número de puerto inválido. %i < <port_num> < %i\n", PORT_MIN, PORT_MAX);
    }
    return portNumber <= PORT_MIN;
}

/**
 * Realiza y verifica que se conecte exitosamente al servidor
 * @return 0 si se conectó correctamente
 */
int32_t tryConnection() {
    struct sockaddr_in *serverAddress = calloc(1, sizeof(struct sockaddr_in));
    serverAddress->sin_family = AF_INET;
    serverAddress->sin_port = htons(portNumber);
    serverAddress->sin_addr = *buf;
    return connect(socketFd, (struct sockaddr *) serverAddress, sizeof(struct sockaddr));
}

/**
 * Envía un mensaje de prueba para testear la conexión con el servidor
 * @return 0 en caso de éxtio, o 1 en caso contrario
 */
int32_t checkConnection() {
    char *buffer = calloc(MAX_MSG_SIZE, sizeof(char));
    send(socketFd, "OK", strlen("OK"), 0);
    int32_t valRead = (int32_t) recv(socketFd, buffer, MAX_MSG_SIZE, 0);
    if (valRead < 1) {
        fprintf(stderr, "Error al verificar la conexión\n");
    } else {
        printf("TEST CONNECTION [%i]: %s\n", valRead, buffer);
    }
    free(buffer);
    return errno || valRead < 1;
}

/**
 * Imprime en pantalla un mensaje de bienvenida
 */
void printWelcomeMsg() {
    printf("\n\n\n------------------------------------------\n");
    printf("|               Bienvenido               |\n");
    printf("------------------------------------------\n");
}

/**
 * Realiza la recepción de mensajes del servidor
 * @return 0 en caso de éxtio, o 1 en caso contrario
 */
int32_t receiveMsg() {
    char *buffer = calloc(MAX_MSG_SIZE, sizeof(char));
    // MSG_WAITALL -> On SOCK_STREAM sockets this requests that the function block until
    // the full amount of data can be returned
    int32_t valRead = (int32_t) recv(socketFd, buffer, MAX_MSG_SIZE, 0);
    if (valRead < 1) {
        fprintf(stderr, "Error al intentar recibir datos\n");
    } else if (checkMD5(buffer)) {
        fprintf(stderr, "Error al validar el checksum del mensaje recibido\n");
    } else {
        if (strncmp(buffer, LOG_KEY, strlen(LOG_KEY)) == 0) {
            getLog(buffer);
        } else {
            printf("Mensaje recibido: %s\n", buffer);
        }
    }
    free(buffer);
    return errno || valRead < 1;
}

/**
 * Compara el checksum del mensaje recibido
 * @param buffer mensaje recibido con formato <msg|checksum>
 * @return 0 en caso de éxito, distinto de 0 en caso contrario
 */
int32_t checkMD5(char *buffer) {
    /* Obtengo el mensaje y el checksum del buffer recibido */
    char *checksum = &strstr(buffer, MD5_KEY)[strlen(MD5_KEY)];
    buffer[strlen(buffer) - strlen(checksum) - strlen(MD5_KEY)] = '\0';

    /* Genero el md5 del mensaje recibido */
    MD5_CTX c;
    unsigned char out[MD5_DIGEST_LENGTH];

    MD5_Init(&c);
    MD5_Update(&c, buffer, strlen(buffer));
    MD5_Final(out, &c);

    /* Comparo los checksum */
    if (checksum == NULL) {
        return 1;
    } else {
        return strncmp(checksum, (char *) out, MD5_DIGEST_LENGTH);
    }
}

/**
 * Recibe las porciones del log y las almacena en un archivo
 * https://idiotdeveloper.com/file-transfer-using-tcp-socket-in-c/
 * @param buffer porción del archivo log recibida
 */
void getLog(char *buffer) {
    /* Elimino la key para identificar el mensaje */
    strtok(buffer, "$");

    if ((fp = fopen("../log/socket.log", "a")) == NULL) {
        fprintf(stderr, "Error al abrir el archivo log del suscriptor\n");
    } else {
        fprintf(fp, "%s", strtok(NULL, "$"));
        fclose(fp);
    }
}

/**
 * Realiza toda la lógica para registrar los handlers de exit()
 */
int32_t registerExitHandlers() {
    signal(SIGINT, intHandler);
    return atexit(exitHandlerSocket);
}

/**
 * Exit handler para cerrar el socket del suscriptor
 */
static void exitHandlerSocket(void) {
    printf("Elimino el socket del suscriptor\n");
    close(socketFd);
}

/**
 * Handler para manejar combinación Ctrl+C
 * https://stackoverflow.com/a/4217070/14311150
 * @param sig señal a ser manejada
 */
void intHandler(int sig) {
    signal(sig, SIG_IGN);
    printf("\nYou hit Ctrl-C. Adieu!...\n");
    exit(EXIT_SUCCESS);
}