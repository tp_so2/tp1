//
// Created by lucas on 8/4/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#define PARAMS                  3
#define MAX_PROD                25
#define MAX_LEN                 100

/* Variables globales */
mqd_t qdProd;
char queueName[MAX_PROD];
u_int16_t msgRate;
u_int16_t msgType;

/* Declaración de funciones */
int32_t checkArgs(int32_t argc, char *argv[]);

int32_t checkMsgType(char *type);

int32_t checkMsgRate(char *rate);

int32_t queueConnection();

void printWelcomeMsg();

int32_t sendGeneratedMsg();

void msgGenerator(char *msg);

void generateDatetime(char *msg);

void generateFreeMem(char *msg);

void generateNormLoad(char *msg);

int32_t registerExitHandlers();

static void exitHandlerQueues(void);

void intHandler(int sig);

/* Función principal */
int32_t main(int argc, char *argv[]) {
    /* Verifico la validez de los argumentos */
    if (checkArgs(argc, argv)) {
        exit(EXIT_FAILURE);
    }

    /* Registro los exit handlers */
    if (registerExitHandlers()) {
        printf("Ocurrió un error al hacer el registro de los exit handlers: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Conexión a la cola de mensajes */
    if (queueConnection()) {
        printf("Ocurrió un error durante la conexión a la cola de mensajes: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    printWelcomeMsg();

    /* Envío de mensajes periódicamente */
    time_t start = time(NULL);
    while (1) {
        if (msgRate <= (time(NULL) - start)) {
            sendGeneratedMsg();
            start = time(NULL);
        }

        if (errno != 0) {
            printf("Ocurrió un error durante la comunicación con el servidor: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }
}

/**
 * Verifica validez de los argumentos con los que se ejecutó el programa
 * @param argc cantidad de argumentos
 * @param argv listado de argumentos
 * @return 0 si los argumentos son correctos
 */
int32_t checkArgs(int argc, char *argv[]) {
    int32_t flag = 0;
    if (argc != PARAMS) {
        fprintf(stderr, "Formato inválido. ./productor <msg_type> <msg_rate>\n");
        flag++;
    } else {
        flag += checkMsgType(argv[1]);
        flag += checkMsgRate(argv[2]);
    }
    return flag;
}

/**
 * Verifica validez del argumento tipo de mensaje
 * @param type tipo de mensaje
 * @return 0 si es válida, 1 en caso contrario
 */
int32_t checkMsgType(char *type) {
    msgType = (u_int16_t) strtol(type, NULL, 10);
    if (errno != 0) {
        printf("Errno: %s\n", strerror(errno));
    }
    if (msgType < 1 || msgType > 3) {
        fprintf(stderr, "Tipo de mensaje inválido. Sólo están implementados los tipos 1, 2 o 3.\n");
    }
    return errno != 0 || msgType < 1 || msgType > 3;
}

/**
 * Verifica validez del argumento tasa de mensajes
 * @param rate tasa de mensajes en segundos
 * @return 0 si es válida, 1 en caso contrario
 */
int32_t checkMsgRate(char *rate) {
    msgRate = (u_int16_t) strtol(rate, NULL, 10);
    if (errno != 0) {
        printf("Errno: %s\n", strerror(errno));
    }
    if (msgRate < 1) {
        fprintf(stderr, "Tasa de mensajes inválida. Sólo se aceptan valores enteros positivos.\n");
    }
    return errno != 0 || msgRate < 1;
}

/**
 * Realiza la conexión a la cola de mensajes del servidor
 * Sólo se conecta, no crea colas. Falla en caso que no exista la cola solicitada
 * @param name nombre de la cola de mensajes a conectarse
 * @return 0 en caso de éxito, o 1 en caso contrario
 */
int32_t queueConnection() {
    sprintf(queueName, "/%i", msgType);

    if ((qdProd = mq_open(queueName, O_WRONLY | O_NONBLOCK)) == -1) {
        if (errno == ENOENT) {
            fprintf(stderr, "No existe ninguna cola de mensajes con nombre [%s].\n", queueName);
        }
    }
    return errno;
}

/**
 * Imprime en pantalla un mensaje de bienvenida
 */
void printWelcomeMsg() {
    printf("\n------------------------------------------\n");
    printf("|        Bienvenido productor [%i]        |\n", msgType);
    printf("------------------------------------------\n\n");
}

/**
 * Envía el mensaje generado a la cola de mensajes
 * @return 0 en caso de éxtio, o 1 en caso contrario
 */
int32_t sendGeneratedMsg() {
    char *msg = calloc(MAX_LEN, sizeof(char));
    msgGenerator(msg);
    printf("[%lu] - Message to send: %s\n", (strlen(msg) + 1), msg);
    if (mq_send(qdProd, msg, strlen(msg) + 1, 0) == -1) {
        fprintf(stderr, "Ocurrió un error al enviar el mensaje: %s\n", msg);
    }
    free(msg);
    return errno;
}

/**
* Genera el mensaje para ser enviado dependiendo el tipo que se pasó por argumento
 *  Mensaje tipo 1: Fecha y hora
 *  Mensaje tipo 2: Memoria libre del sistema
 *  Mensaje tipo 3: Load del sistema normalizado
 *  @param buffer que almacena el mensaje a ser enviado
*/
void msgGenerator(char *msg) {
    switch (msgType) {
        case 1:
            generateDatetime(msg);
            break;
        case 2:
            generateFreeMem(msg);
            break;
        case 3:
            generateNormLoad(msg);
            break;
        default:
            snprintf(msg, MAX_LEN, "Tipo de mensaje no implementado\n");
            break;
    }
}

/**
 * Genera el mensaje tipo 1 (Fecha y hora)
 * @param puntero a string con los datos de fecha y hora actuales
 */
void generateDatetime(char *msg) {
    time_t t;
    time(&t);
    snprintf(msg, MAX_LEN, "Datetime: %s", strtok(ctime(&t), "\n"));
}

/**
 * Genera el mensaje tipo 2 (Memoria libre del sistema)
 * @param puntero a string con los datos de memoria libre del sistema
 */
void generateFreeMem(char *msg) {
    /* Alternativa 1 */
//    snprintf(buff, 100, "%li", get_avphys_pages() * sysconf(_SC_PAGESIZE)/1024);
    /* Alternativa 2 */
    struct sysinfo info;
    sysinfo(&info);
    snprintf(msg, MAX_LEN, "Free RAM: %li kB", info.freeram / 1024);
}

/**
 * Genera el mensaje tipo 3 (Load normalizado del sistema)
 * @param puntero a string con los datos de load normalizado del sistema
 */
void generateNormLoad(char *msg) {
    /* Alternativa 1 */
//    double load[3];
//    getloadavg(load, 3);
//    snprintf(buff, 100, "Normalized load: %f", load[0] / sysconf(_SC_NPROCESSORS_ONLN));
    /* Alternativa 2 */
    struct sysinfo info;
    sysinfo(&info);
    snprintf(msg, MAX_LEN, "Normalized load: %lu", (long) info.loads[0] / sysconf(_SC_NPROCESSORS_ONLN));
}

/**
 * Realiza toda la lógica para registrar los handlers de exit()
 */
int32_t registerExitHandlers() {
    signal(SIGINT, intHandler);
    return atexit(exitHandlerQueues);
}

/**
 * Exit handler para cerrar el descriptor de la cola de mensajes
 */
static void exitHandlerQueues(void) {
    printf("Cierro la cola\n");
    close(qdProd);
}

/**
 * Handler para manejar combinación Ctrl+C
 * https://stackoverflow.com/a/4217070/14311150
 * @param sig señal a ser manejada
 */
void intHandler(int sig) {
    signal(sig, SIG_IGN);
    printf("\nYou hit Ctrl-C. Aufwiedersehen!...\n");
    exit(EXIT_SUCCESS);
}