//
// Created by lucas on 13/4/21.
// https://stackoverflow.com/a/6509569/14311150
//

#include "../lib/logger.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

FILE *fp;

void logger(const int16_t src, const char *ip, const uint16_t port, const char *message) {
    fp = fopen("../log/dm.log", "a+");
    time_t t;
    time(&t);
    fprintf(fp, "[%s] | Src: [%i] -> Dst: [%s:%i] | Msg: [%s]\n", strtok(ctime(&t),"\n"), src, ip, port, message);
    fclose(fp);
}